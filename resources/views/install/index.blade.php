@extends('layouts.install', ['no_header' => 1])
@section('title', 'Welcome - POS Installation')

@section('content')
<div class="container">
    
    <div class="row">
      <h3 class="text-center">{{ config('app.name', 'POS') }} Installation <small>Step 1 of 3</small></h3>

        <div class="col-md-8 col-md-offset-2">
          <hr/>
          @include('install.partials.nav', ['active' => 'install'])

          <div class="box box-primary active">
            <!-- /.box-header -->
            <div class="box-body">
              <h3 class="text-success">
                Selamat datang di Instalasi POS!
              </h3>
              <p><strong class="text-danger">[IMPORTANT]</strong> Sebelum Anda mulai menginstal, pastikan Anda memiliki informasi berikut yang sudah Anda siapkan :</p>

              <ol>
                <li>
                  <b>Dokumen Langkah Install</b> - <a href="https://premiumpos-docs.dndsoft.my.id" target="_blank">Documentation</a>
                </li>
                <li>
                  <b>Nama Aplikasi</b> - Sesuatu yang pendek & Berarti.
                </li>
                <li>
                  <b>Informasi Database</b>
                  <ul>
                    <li>Username</li>
                    <li>Password</li>
                    <li>Database Name</li>
                    <li>Database Host</li>
                  </ul>
                </li>
                <li>
                  <b>Konfigurasi Email</b> - Detil SMTP (optional)
                </li>
                
              </ol>

              

              
              
              <a href="{{route('install.details')}}" class="btn btn-primary pull-right">Next</a>
            </div>
          <!-- /.box-body -->
          </div>

        </div>

    </div>
</div>
@endsection
