@php 
    $colspan = 15;
    $custom_labels = json_decode(session('business.custom_labels'), true);
@endphp
<div class="table-responsive">
    <table class="table table-bordered table-striped ajax_view hide-footer" id="product_table">
        <thead>
            <tr>
                <th><input type="checkbox" id="select-all-row" data-table-id="product_table"></th>
                <th>@lang('sale.product')</th>
                <th>@lang('product.sku')</th>
                <th>@lang('purchase.business_location') @show_tooltip(__('lang_v1.product_business_location_tooltip'))</th>
                @can('access_default_selling_price')
                    @php 
                        $colspan++;
                    @endphp
                    <th>@lang('lang_v1.selling_price')</th>
                @endcan
                <th>@lang('report.current_stock')</th>
                <th>@lang('product.product_type')</th>
                <th>@lang('product.category')</th>
            </tr>
        </thead>
    </table>
</div>