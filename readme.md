## About Premium POS

Premium POS is a POS application by [dndsoft](https://dndsoft.my.id).

## Installation & Documentation
You will find installation guide and documentation in the downloaded zip file.
Also, For complete updated documentation of the Premium POS please visit online [documentation guide](https://premiumpos-docs.dndsoft.my.id).


